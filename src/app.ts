import * as fs from 'fs';
import * as es from 'event-stream';

import * as _ from "lodash";

import * as BT from "./tree"

import Context = require("./context");
import NLU = require("./nlu");

function assembleBT() {
    let theTree = new BT.BehaviorTree();
    let Welcome = new BT.Message("Welcome! What we want to do?");
    let Service = new BT.Message("My Services are... ");
    let Unknown = new BT.Message("What? ");
    //let cswitch = new BT.ContextSwitcher([Welcome,Service, Unknown],["empty", "land.service", "unknown"]);
    //theTree.root = cswitch;
    return theTree;
}

async function main() {
    let tree = await BT.TreeFactory.fromXML("./examples/tree.json");
    //console.log(tree.behaviortree.selector);

    let TheTree = tree;

    TheTree.execute();

    process.stdin
        .pipe(es.split('\n'))
        .on('data', parseCommand);

    function parseCommand(command) {
        let new_context = NLU.parseUserInput(command);
        console.log(`NEW CONTEXT: ${new_context}`);
        Context.active_context = new_context;
        TheTree.execute();
    }
}

main();

