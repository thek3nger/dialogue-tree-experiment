import * as fs from "fs";

export function readFile(filename: string): Promise<string> {
    return new Promise(
        (resolve, reject) => {
            fs.readFile(filename, (error, result) => {
                if (error) reject(error);
                resolve(result);
            });
        }
    );
}