import * as fs from 'fs';

import * as _ from "lodash";

import * as utils from "./utility";

import Context = require("./context");

enum BTState { SUCCESS, FAILURE, IN_PROGRESS }

interface BTNode {
    children: BTNode[];
    guard: string;
    execute(): BTState;
}

// Behavior Tree main class.
export class BehaviorTree {
    root: BTNode;
    constructor(tree_node = undefined) {
        if (tree_node == undefined) {
            this.root = null;
        } else {
            this.root = NodeFactory.fromObject(tree_node);
        }
    }

    execute(): BTState {
        return this.root.execute();
    }
}

// Sequence: return a failure at the first failure.
export class Sequence implements BTNode {

    children: BTNode[];
    guard: string;

    constructor(guard = null, subtree = undefined) {
        this.guard = guard;
        if (subtree == undefined) {
            this.children = [];
        } else {
            this.children = _.map(subtree, NodeFactory.fromObject);
        }
    }

    execute(): BTState {
        if (this.guard !== null && Context.active_context !== this.guard) return BTState.FAILURE;
        for (let c of this.children) {
            let state = c.execute();
            if (state === BTState.FAILURE) return BTState.FAILURE;
        }
        return BTState.SUCCESS;
    }
}

// Select: return a success at the first success.
export class Select implements BTNode {
    children: BTNode[];
    guard: string;

    constructor(guard = null, subtree = undefined) {
        this.guard = guard;
        if (subtree == undefined) {
            this.children = [];
        } else {
            this.children = _.map(subtree, NodeFactory.fromObject);
        }
    }

    execute(): BTState {
        if (this.guard !== null && Context.active_context !== this.guard) return BTState.FAILURE;
        for (let c of this.children) {
            let state = c.execute();
            if (state === BTState.SUCCESS) return BTState.SUCCESS;
        }
        return BTState.FAILURE;
    }
}

// export class ContextSwitcher implements BTNode {
//     children: BTNode[];
//     contexts: string[];

//     constructor(children, contexts) {
//         this.contexts = contexts;
//         this.children = children;
//     }

//     execute(): BTState {
//         for (let i = 0; i < this.contexts.length; i++) {
//             if (Context.active_context === this.contexts[i]) {
//                 return this.children[i].execute();
//             }
//         }
//         return BTState.FAILURE;
//     }
// }

export class Message implements BTNode {
    children: BTNode[];
    guard: string;
    message: string;

    constructor(message: string) {
        this.guard = null;
        this.message = message;
    }

    execute(): BTState {
        console.log(this.message);
        return BTState.SUCCESS;
    }
}

export class TreeFactory {
    static async fromXML(filename: string) {
        let json_tree = await utils.readFile(filename);
        let json_parsed = JSON.parse(json_tree);
        let BT = new BehaviorTree(json_parsed.root);
        console.log(JSON.stringify(BT));
        return BT;
    }
}

export class NodeFactory {
    static fromObject(subtree: any): BTNode {
        switch (subtree.type) {
            case "selector":
                return new Select(subtree.guard, subtree.children);
            case "sequence":
                return new Sequence(subtree.guard, subtree.children);
            case "message":
                return new Message(subtree.message);
        }
    }
}