import * as _ from "lodash";

import Context = require("./context");

class NLU {

    parseUserInput(input: string) {
        if (Context.active_context === "empty" || Context.active_context === "unknown") {
            switch (input.trim()) {
                case "service":
                    return "land.service";
                case "price":
                    return "land.price";
                default:
                    return "unknown";
            }
        }
        if (Context.active_context === "land.service") {
            switch (input.trim()) {
                case "loyalty":
                    return "land.service.loyalty"
                case "other":
                    return "land.service.other"
                default:
                    return "unknown";
            }
        }
        return "unknown";
    }
}

export = new NLU();